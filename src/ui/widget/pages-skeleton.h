// SPDX-License-Identifier: GPL-2.0-or-later
/**
 * @file
 * List of paper sizes
 */
/*
 * Authors:
 *   bulia byak <buliabyak@users.sf.net>
 *   Lauris Kaplinski <lauris@kaplinski.com>
 *   Jon Phillips <jon@rejon.org>
 *   Ralf Stephan <ralf@ark.in-berlin.de> (Gtkmm)
 *   Bob Jamison <ishmal@users.sf.net>
 *   Abhishek Sharma
 *   + see git history
 *
 * Copyright (C) 2000 - 2018 Authors
 *
 * Released under GNU GPL v2+, read the file 'COPYING' for more information.
 */

#ifndef SEEN_PAGES_SKELETON_H
#define SEEN_PAGES_SKELETON_H


    /** \note
     * The ISO page sizes in the table below differ from ghostscript's idea of page sizes (by
     * less than 1pt).  Being off by <1pt should be OK for most purposes, but may cause fuzziness
     * (antialiasing) problems when printing to 72dpi or 144dpi printers or bitmap files due to
     * postscript's different coordinate system (y=0 meaning bottom of page in postscript and top
     * of page in SVG).  I haven't looked into whether this does in fact cause fuzziness, I merely
     * note the possibility.  Rounding done by extension/internal/ps.cpp (e.g. floor/ceil calls)
     * will also affect whether fuzziness occurs.
     *
     * The remainder of this comment discusses the origin of the numbers used for ISO page sizes in
     * this table and in ghostscript.
     *
     * The versions here, in mm, are the official sizes according to
     * <a href="http://en.wikipedia.org/wiki/Paper_sizes">http://en.wikipedia.org/wiki/Paper_sizes</a>
     * at 2005-01-25.  (The ISO entries in the below table
     * were produced mechanically from the table on that page.)
     *
     * (The rule seems to be that A0, B0, ..., D0. sizes are rounded to the nearest number of mm
     * from the "theoretical size" (i.e. 1000 * sqrt(2) or pow(2.0, .25) or the like), whereas
     * going from e.g. A0 to A1 always take the floor of halving -- which by chance coincides
     * exactly with flooring the "theoretical size" for n != 0 instead of the rounding to nearest
     * done for n==0.)
     *
     * Ghostscript paper sizes are given in gs_statd.ps according to gs(1).  gs_statd.ps always
     * uses an integer number ofpt: sometimes gs_statd.ps rounds to nearest (e.g. a1), sometimes
     * floors (e.g. a10), sometimes ceils (e.g. a8).
     *
     * I'm not sure how ghostscript's gs_statd.ps was calculated: it isn't just rounding the
     * "theoretical size" of each page topt (see a0), nor is it rounding the a0 size times an
     * appropriate power of two (see a1).  Possibly it was prepared manually, with a human applying
     * inconsistent rounding rules when converting from mm to pt.
     */
    /** \todo
     * Should we include the JIS B series (used in Japan)
     * (JIS B0 is sometimes called JB0, and similarly for JB1 etc)?
     * Should we exclude B7--B10 and A7--10 to make the list smaller ?
     * Should we include any of the ISO C, D and E series (see below) ?
     */



    /* See http://www.hbp.com/content/PCR_envelopes.cfm for a much larger list of US envelope
       sizes. */
    /* Note that `Folio' (used in QPrinter/KPrinter) is deliberately absent from this list, as it
       means different sizes to different people: different people may expect the width to be
       either 8, 8.25 or 8.5 inches, and the height to be either 13 or 13.5 inches, even
       restricting our interpretation to foolscap folio.  If you wish to introduce a folio-like
       page size to the list, then please consider using a name more specific than just `Folio' or
       `Foolscap Folio'. */

static char const pages_skeleton[] =
"#comma-separated : NAME - WIDTH - HEIGHT - UNIT; name and unit must have no spacing before or after\n"
"A4,                210,  297,mm\n"
"US Letter,         8.5,   11,in\n"
"US Legal,          8.5,   14,in\n"
"US Executive,     7.25, 10.5,in\n"
"A0,                841, 1189,mm\n"
"A1,                594,  841,mm\n"
"A2,                420,  594,mm\n"
"A3,                297,  420,mm\n"
"A5,                148,  210,mm\n"
"A6,                105,  148,mm\n"
"A7,                 74,  105,mm\n"
"A8,                 52,   74,mm\n"
"A9,                 37,   52,mm\n"
"A10,                26,   37,mm\n"
"B0,               1000, 1414,mm\n"
"B1,                707, 1000,mm\n"
"B2,                500,  707,mm\n"
"B3,                353,  500,mm\n"
"B4,                250,  353,mm\n"
"B5,                176,  250,mm\n"
"B6,                125,  176,mm\n"
"B7,                 88,  125,mm\n"
"B8,                 62,   88,mm\n"
"B9,                 44,   62,mm\n"
"B10,                31,   44,mm\n"
"C0,                917, 1297,mm\n"
"C1,                648,  917,mm\n"
"C2,                458,  648,mm\n"
"C3,                324,  458,mm\n"
"C4,                229,  324,mm\n"
"C5,                162,  229,mm\n"
"C6,                114,  162,mm\n"
"C7,                 81,  114,mm\n"
"C8,                 57,   81,mm\n"
"C9,                 40,   57,mm\n"
"C10,                28,   40,mm\n"
"D1,                545,  771,mm\n"
"D2,                385,  545,mm\n"
"D3,                272,  385,mm\n"
"D4,                192,  272,mm\n"
"D5,                136,  192,mm\n"
"D6,                 96,  136,mm\n"
"D7,                 68,   96,mm\n"
"E3,                400,  560,mm\n"
"E4,                280,  400,mm\n"
"E5,                200,  280,mm\n"
"E6,                140,  200,mm\n"
"CSE,               462,  649,pt\n"
"US #10 Envelope,   9.5,4.125,in\n"
"DL Envelope,       220,  110,mm\n"
"Ledger/Tabloid,     11,   17,in\n"
"Banner 468x60,     468,   60,px\n"
"Icon 16x16,         16,   16,px\n"
"Icon 32x32,         32,   32,px\n"
"Icon 48x48,         48,   48,px\n"
"Business Card (ISO 7810), 85.60,53.98,mm\n"
"Business Card (US),    3.5,2,in\n"
"Business Card (Europe),        85,    55,mm\n"
"Business Card (Aus/NZ),        90,    55,mm\n"
"Arch A,         9,    12,in\n"
"Arch B,        12,    18,in\n"
"Arch C,        18,    24,in\n"
"Arch D,        24,    36,in\n"
"Arch E,        36,    48,in\n"
"Arch E1,       30,    42,in\n";














#endif
